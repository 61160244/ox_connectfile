/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows
 */
public class PlayerService {

    static Player o, x;

    static {
        o = new Player('O');
        x = new Player('X');
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
//        ArrayList<Player> player = new ArrayList();
        try {
            file = new File("OX.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            PlayerService.o = (Player) ois.readObject();
            PlayerService.x = (Player) ois.readObject();
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
//        player.add(PlayerService.o);
//        player.add(PlayerService.x);
//        return player;

    }

    public static void save(Player o, Player x) {

        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("OX.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.writeObject(x);
        } catch (IOException ex) {
            Logger.getLogger(PlayerService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static Player getO() {
        return o;
    }

    public static Player getX() {
        return x;
    }
    
}

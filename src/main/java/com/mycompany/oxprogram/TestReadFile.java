/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxprogram;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows
 */
public class TestReadFile {

    public static void main(String[] args) {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Player o = null;
        Player x = null;
        try {
            file = new File("OX.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            o = (Player) ois.readObject();
            x = (Player) ois.readObject();
        } catch (IOException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestReadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(o.getName());
        System.out.println(x.getName());

    }

}
